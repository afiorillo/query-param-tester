# Query Parameter Tester

A little Saturday morning project to actually test query parameter limitations.
Just open the `index.html` in your browser (or host it somewhere) and read the text.
Alternatively, go to the [Gitlab Page](https://afiorillo.gitlab.io/query-param-tester/?target=0&body=).
For example:

```bash
$ chromium index.html
```